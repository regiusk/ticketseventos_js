package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.laboratorioModel;

import static br.pucpr.bsi.prog3.ticketsEventosBSI.tests.enums.ArtistaEnum.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Artista;
import br.pucpr.bsi.prog3.ticketsEventosBSI.tests.enums.ArtistaEnum;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

@RunWith(Parameterized.class)
public class TesteArtistaModel {

	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////		
	protected ArtistaEnum artistaEnum;
	protected Artista artista;

	@SuppressWarnings("rawtypes")
	@Parameterized.Parameters
	public static Collection listaObjetos() {
		return Arrays.asList(new Object[][] {
				{ FRANCIS_BACON },
				{ LEON_BAKST },
				{ JOHN_BALDESSARI },
				{ HANS_BALDUNG },
				{ MIROSLAW_BALKA },
				{ GIACOMO_BALLA },
				{ BALTHUS }
		});
	}	
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	
	public TesteArtistaModel(ArtistaEnum artistaEnum) {
		this.artistaEnum = artistaEnum;
	}
	
	
	//////////////////////////////////////////
	// METODOS AUXILIARES
	//////////////////////////////////////////	
	
	/**
	 * Metodo para criar uma nova Cia Aerea
	 * @return
	 */
	public static Artista novoArtista(ArtistaEnum ArtistaEnum){
		Artista Artista = new Artista();
		Artista.setNome(ArtistaEnum.getNome());
		return Artista;
	}
	
	@Before
	public void criaArtista(){
		this.artista = novoArtista(this.artistaEnum);
	}
	
	//////////////////////////////////////////
	// METODO DE TESTE JUNIT
	//////////////////////////////////////////
	
	@Test
	public void criar() {
		//Verifica se nao eh nulo
		Assert.assertNotNull(artista);
		
		//Verifica se os atributos estao preenchidos
		Assert.assertFalse(artista.getNome().isEmpty());
		
		//Alteracao dos atributos
		ArtistaEnum artistaEnumTemp = ArtistaEnum.getArtista();
		artista.setNome(artistaEnumTemp.getNome());
		
		//Verifica se alteracao dos atributos esta sendo realizada
		Assert.assertEquals(artista.getNome(), artistaEnumTemp.getNome());
		
		//Volta os atributos, para ficar compativel com outros testes		
		artista.setNome(this.artistaEnum.getNome());
	}
}
