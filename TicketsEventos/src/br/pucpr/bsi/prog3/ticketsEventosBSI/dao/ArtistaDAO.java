package br.pucpr.bsi.prog3.ticketsEventosBSI.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.dao.utils.Conexao;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.BSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Artista;

public class ArtistaDAO extends PatternDAO<Artista> {

	//////////////////////////////////////////
	// QUERIES
	//////////////////////////////////////////
	
	private static StringBuilder insertSQL = new StringBuilder()
		.append("INSERT INTO ARTISTA ")
		.append("	(NOME) ")
		.append("VALUES ")
		.append("	(?)");
	
	private static StringBuilder updateSQL = new StringBuilder()
		.append("UPDATE ARTISTA ")
		.append("SET NOME = ? ")
		.append("WHERE ID = ? ");
	
	private static StringBuilder deleteSQL = new StringBuilder()
		.append("DELETE FROM ARTISTA ")
		.append("WHERE ID = ? ");
	
	private static StringBuilder selectIdSQL =  new StringBuilder()
		.append("SELECT ID, NOME ")
		.append("FROM ARTISTA ")
		.append("WHERE ID = ? ");
	
	private static StringBuilder selectAllSQL =  new StringBuilder()
		.append("SELECT ID, NOME ")
		.append("FROM ARTISTA ");
	
	private static ArtistaDAO instance = new ArtistaDAO();
	
	private ArtistaDAO() {
	}

	public static ArtistaDAO getInstance() {
		return instance;
	}

	/////////////////////////////////////////
	// METODOS DML COM ALTERACAO NA BASE
	/////////////////////////////////////////
	
	@Override
	public Long insert(Artista object) {
		int cont = 1;
		Connection connection = Conexao.getInstance().obterConexao();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(insertSQL.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(cont++, object.getNome());
			
			ps.executeUpdate();
			//Obtem o ID gerado pelo banco HSQLDB
			object.setId(retrievePrimaryKeygenerated(rs, ps));
		} catch (SQLException e) {
			e.printStackTrace();
			throw new BSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return object.getId();
	}

	@Override
	public boolean update(Artista object) {
		int cont = 1;
		Connection connection = Conexao.getInstance().obterConexao();
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(updateSQL.toString());
			ps.setString(cont++, object.getNome());
			ps.setLong(cont++, object.getId());
			
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new BSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(null, ps, connection);
 		}
		return true;
	}

	@Override
	public boolean delete(Artista object) {
		int cont = 1;
		Connection connection = Conexao.getInstance().obterConexao();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(deleteSQL.toString());
			ps.setObject(cont++, object.getId());
			
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new BSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return true;
	}
	
	/////////////////////////////////////////
	// METODOS DML DE RECUPERACAO DE INFORMACAO
	/////////////////////////////////////////	
	
	@Override
	public Artista findById(Long id) {
		int cont = 1;
		Connection connection = Conexao.getInstance().obterConexao();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Artista artista = null;
		try {
			ps = connection.prepareStatement(selectIdSQL.toString());
			ps.setObject(cont++, id);
			rs = ps.executeQuery();
			if(rs.next()){
				artista = populateObject(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new BSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return artista;
	}
	
	/**
	 * Metodo utilizado para preparar o SQL de acordo com as informacoes contidas no filtro
	 * @param filtro
	 * @return
	 */
	private String prepareStringSQLForFilter(Artista filtro){
		StringBuffer sb = new StringBuffer(selectAllSQL.toString());
		
		//Eh possivel inserir o WHERE direto, pois foi realizada a validacao de que pelo 
		//menos uma informacao foi preenchida no filtro
		sb.append(" WHERE ");
		
		if(filtro.getNome() != null){
			sb.append(" NOME = ? ");
		}
		return sb.toString();
	}
	
	/**
	 * Metodo que prepara o statment de acordo com o filtro a ordem de insercao dos parametros
	 * que foi inserida no metodo prepareStringSQLForFilter deve ser respeitada aqui
	 * @param filtro
	 * @param ps
	 * @throws SQLException
	 */
	private void prepareStatementForFilter(Artista filtro, PreparedStatement ps) throws SQLException{
		int cont = 1;
		if(filtro.getNome() != null){
			ps.setObject(cont++, filtro.getNome());
		}
	}
	
	public List<Artista> findByFilter(Artista filtro) {
		
		Connection connection = Conexao.getInstance().obterConexao();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Artista> artistas = new ArrayList<Artista>();
		try {
			ps = connection.prepareStatement(prepareStringSQLForFilter(filtro).toString());
			prepareStatementForFilter(filtro, ps);
			rs = ps.executeQuery();
			while(rs.next()){
				artistas.add(populateObject(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new BSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return artistas;
	}
	
	
	@Override
	public List<Artista> findAll() {
		Connection connection = Conexao.getInstance().obterConexao();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Artista> artistas = new ArrayList<Artista>();
		try {
			ps = connection.prepareStatement(selectAllSQL.toString());
			rs = ps.executeQuery();
			while(rs.next()){
				artistas.add(populateObject(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new BSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return artistas;
	}
	
	@Override
	protected Artista populateObject(ResultSet rs) throws SQLException {
		Artista artista = new Artista();
		artista.setId(rs.getLong("ID"));
		artista.setNome(rs.getString("NOME"));
		return artista;
	}
	
	
}