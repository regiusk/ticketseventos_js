package br.pucpr.bsi.prog3.ticketsEventosBSI.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

/**
 * Classe que abrigarah o menu do sistema. Realizarah tambem o papel da JDesktopPane
 * para o abrigo as telas de JInternalPane
 * @author Mauda
 *
 */

public class PrincipalView extends JFrame {

	private static final long serialVersionUID = -4945333243653799172L;

	private JDesktopPane desktop;
	
	/**
	 * Construtor da classe
	 */
	public PrincipalView() {
		super("Tickets Aereos BSI");
		iniciarDesktopPane();
		criarMenu();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Ajusta para a 83% da area util do desktop
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setSize((int)(screenSize.getWidth()/1.2), (int)(screenSize.getHeight()/1.2));
	}
	
	/**
	 * Instancia o DesktopPane, o qual eh utilizado para abrir as JInternalFrame
	 */
	private void iniciarDesktopPane() {
		desktop = new JDesktopPane();
		desktop.setBackground(Color.DARK_GRAY);
		getContentPane().add(desktop);
	}
	
	/**
	 * Metodo responsavel por criar o menu do sistema
	 */
	private void criarMenu(){
		JMenuBar menuBar = new JMenuBar();
		//Adiciona o JMenuBar ao JFrame da tela 
		setJMenuBar(menuBar);
		
		//Adiciona o JMenu, que eh uma coluna do menu
		JMenu menuCadastro = new JMenu("Cadastro Informacoes");
		menuCadastro.setMnemonic(KeyEvent.VK_C);
		menuBar.add(menuCadastro);
		
		//Adiciona um item ao menu criado
		JMenuItem itemArtista = new JMenuItem("Artista");
		itemArtista.setMnemonic(KeyEvent.VK_R);
		itemArtista.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));		
		itemArtista.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent evt) {
				ArtistaView artistaView = new ArtistaView();
				desktop.add(artistaView);
			}

		});
		menuCadastro.add(itemArtista);
		
		
		//TODO Adicionar os outros menus e itens de menu aqui
	}
	
	/**
	 * Metodo main pra iniciar a execucao do sistema
	 * @param args
	 */
	public static void main(String[] args) {
		final PrincipalView frame = new PrincipalView();
		frame.setVisible(true);
		
		//Essa parte eh utilizada para obter qualquer excecao gerada no sistema e
		//apresentar ao usuario. Experimente comentar esse codigo e ver o que
		//acontece ao gerar um erro
		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
		    public void uncaughtException(Thread t, Throwable e) {
		    	//Imprime a stack
		    	e.printStackTrace();
		    	
		    	//Mostra uma popup com a mensagem para o modo grafico do JavaSwing
		    	String message = e.getMessage();
		    	if(message.isEmpty()){
		    		message = "Erro! Favor verificar o console da aplicacao!";
		    	}
	    		JOptionPane.showMessageDialog(frame, message);
		    }
		});
	}

}
